
import com.codeborne.selenide.Configuration;
import io.qameta.allure.selenide.AllureSelenide;
import listeners.TestNGListener;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import steps.MainSteps;
import steps.RequestsPageSteps;
import steps.SearchResultsSteps;

import static com.codeborne.selenide.logevents.SelenideLogger.addListener;
import static test_utils.Property.getPropertyByName;

@Listeners({TestNGListener.class})
public class test {

    private MainSteps mainSteps;
    private SearchResultsSteps searchResultsSteps;
    private RequestsPageSteps requestsPageSteps;

    @BeforeClass
    private void setUpSelenide() {
        Configuration.browser = getPropertyByName("browser");
        Configuration.startMaximized = true;
        Configuration.timeout = 9000;
        Configuration.holdBrowserOpen = false;
        Configuration.screenshots = false;
        Configuration.savePageSource = false;

        addListener("AllureSelenide", new AllureSelenide());
    }

    @BeforeClass
    private void setUp()
    {
        mainSteps = new MainSteps();

    }

    @Test(description = "Создание запроса и проверка его отображения в списке всех запросов")
    public void checkRequestWasLoggedTest() {
        mainSteps.navigateToMainPage();
        mainSteps.createRequestWithParameters(25, "razrazraz");
        searchResultsSteps = new SearchResultsSteps();
        searchResultsSteps.toMainPage();
        mainSteps.toRequestsPage();
        requestsPageSteps = new RequestsPageSteps();
        requestsPageSteps.checkRequestDisplayed("razrazraz");
    }

    @Test(description = "Проверка сортировки запросов по дате и времени")
    public void checkRequestsSortingTest() {
        mainSteps.navigateToMainPage();
        mainSteps.toRequestsPage();
        requestsPageSteps = new RequestsPageSteps();
        requestsPageSteps.checkRequestsSorted();
    }

    @Test(description = "Проверка отключения сортировки")
    public void checkRequestsNotSortingTest() {
        mainSteps.navigateToMainPage();
        mainSteps.toRequestsPage();
        requestsPageSteps = new RequestsPageSteps();
        requestsPageSteps.checkRequestsNotSorted();
    }

    @DataProvider(name = "requestsProvider")
    public static Object[][] parameters() {
        return new Object[][]{
                {25, "ajax json"},
                {50, "json"},
                {100, "  "}};
    }

    @Test(description = "Проверка поиска по ключевым словам", dataProvider = "requestsProvider")
    public void checkKeywordsSearchTest(int count, String request) {
        mainSteps.navigateToMainPage();
        mainSteps.createRequestWithParameters(count, request);
        searchResultsSteps = new SearchResultsSteps();
        searchResultsSteps.checkResultsCount(count);
        searchResultsSteps.checkRequestByKeywords(request);
    }

    @DataProvider(name = "requestsProviderNegativeTest")
    public static Object[][] fieldsFillingParameters() {
        return new Object[][]{
                {null, null},
                {null, "java"},
                {25, null}};
    }

    @Test(description = "Проверка обязательности заполнения полей при создании запроса", dataProvider = "requestsProviderNegativeTest")
    public void checkFieldsFilled(Integer count, String request) {
        mainSteps.navigateToMainPage();
        mainSteps.createRequestWithParameters(count, request);
        mainSteps.checkErrorMessageExists("Не переданы обязательные параметры");
    }
}
