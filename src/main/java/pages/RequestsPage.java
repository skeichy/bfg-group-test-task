package pages;

import com.codeborne.selenide.*;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.$$;

public class RequestsPage {

    @FindBy(xpath = ".//a[@href='/']/button")
    private SelenideElement toMainPageButtonLocator;

    @FindBy(xpath = ".//button[@name='sort' and @value='True']")
    private SelenideElement sortByDateButtonLocator;

    @FindBy(xpath = ".//button[@name='sort']")
    private SelenideElement doNotSortButtonLocator;

    public ElementsCollection getAllDisplayedRequestsTexts() {
       return $$(By.xpath(".//table/tbody/tr/td[1]"));
    }

    public ElementsCollection getAllDisplayedRequestsDateTime() {
        return $$(By.xpath(".//table/tbody/tr/td[2]"));
    }

    @Step("Клик по кнопке 'Вернуться назад', возвращение к домашней странице")
    public void toMainPageButtonClick() {
        toMainPageButtonLocator.click();
    }

    @Step("Клик по кнопке 'Сортировать по дате'")
    public void clickSortByDate() {
        sortByDateButtonLocator.click();
        Selenide.confirm();
    }

    @Step("Клик по кнопке 'Не сортировать'")
    public void clickDoNotSortButton() {
        doNotSortButtonLocator.click();
        Selenide.confirm();
    }


}
