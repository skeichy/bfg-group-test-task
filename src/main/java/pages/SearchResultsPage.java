package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.$$;

public class SearchResultsPage {

    @FindBy(xpath = ".//a[@href='/']/button")
    private SelenideElement toMainPageButtonLocator;

    public ElementsCollection getAllDisplayedSearchResultsTexts() {
        return $$(By.xpath(".//table/tbody/tr/td[2]"));
    }

    @Step("Клик по кнопке 'Вернуться назад', возвращение к домашней странице")
    public void toMainPageButtonClick() {
        toMainPageButtonLocator.click();
    }
}
