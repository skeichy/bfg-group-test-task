package pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

public class MainPage {

    @FindBy(xpath = ".//input[@id = 'intitle']")
    private SelenideElement requestInputLocator;

    @FindBy(xpath = ".//input[@type='Submit']")
    private SelenideElement searchButtonLocator;

    @FindBy(xpath = ".//select[@id = 'page_number']")
    private SelenideElement resultCountSelectLocator;

    @FindBy(xpath = ".//a[@href='/show_all']/button")
    private SelenideElement showAllButtonLocator;

    @FindBy(xpath = ".//div[@role='alert']")
    public SelenideElement alertMessageLocator;

    @Step("Ввод {count} в поле 'Кол-во...'")
    public void setResultsCount(Integer count) {
        if (count == null) {
            return;
        }
        resultCountSelectLocator.selectOptionByValue(String.valueOf(count));
    }


    @Step("Ввод запроса с текстом {0} в поле 'Введите ключевое слово'")
    public void setRequestValue(String value) {
        requestInputLocator.setValue(value);
    }

    @Step("Нажатие кнопки 'Search', переход к странице результатов поиска")
    public void clickSearchButton() {
        searchButtonLocator.click();
    }

    @Step("Нажатие кнопки 'Показать все запросы'")
    public void clickShowAllButton() {
        showAllButtonLocator.click();
    }

}
