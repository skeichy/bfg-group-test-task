package test_utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Property {

    public static String getPropertyByName(String propertyName) {
        try {
            Properties props = new Properties();
            props.load(new FileInputStream(new File("src/main/resources/config.txt")));
            return props.getProperty(propertyName);

        } catch (final IOException cause) {
            throw new RuntimeException("Ошибка при чтении файла настроек", cause);
        }
    }
}
