package listeners;

import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;


public class TestNGListener implements ITestListener {

    @Attachment(value = "Page screenshot", type = "image/png")
    protected byte[] saveAllureScreenshot() {
        return ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        System.out.println(String.format("Starting test '%s'", iTestResult.getName()));
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        System.out.println(String.format("Test '%s' completed", iTestResult.getName()));
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        System.out.println(String.format("Test '%s' is failed with error", iTestResult.getName()));
        saveAllureScreenshot();
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        System.out.println(String.format("Test '%s' skipped", iTestResult.getName()));
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
        System.out.println("Running tests...");
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        System.out.println(String.format("Starting test '%s'", iTestContext.getName()));
    }
}
