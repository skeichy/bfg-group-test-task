package steps;

import com.codeborne.selenide.*;
import io.qameta.allure.Step;
import org.testng.asserts.SoftAssert;
import pages.MainPage;
import pages.RequestsPage;
import pages.SearchResultsPage;
import test_utils.Property;
import org.testng.Assert;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class MainSteps {

    private MainPage mainPage;
    private SearchResultsPage searchResultsPage;
    private RequestsPage requestsPage;


    @Step("Переход к главной странице")
    public void navigateToMainPage() {
        Selenide.open(Property.getPropertyByName("mainURL"));

        mainPage = page(MainPage.class);
        searchResultsPage = page(SearchResultsPage.class);
        requestsPage = page(RequestsPage.class);

        $("title").shouldHave(attribute("text", "StackOverSearch - новый запрос"));
    }

    @Step("Переход к странице с запросами")
    public void toRequestsPage() {
        mainPage.clickShowAllButton();
        Selenide.confirm();
    }

    @Step("Создание запроса с текстом {1}, кол-во результатов на странице - {0}")
    public void createRequestWithParameters(Integer count, String request) {
        mainPage.setResultsCount(count);
        mainPage.setRequestValue(request);
        mainPage.clickSearchButton();
    }

    @Step("Проверка отображения об ошибке с текстом '{0}' при заполнении полей")
    public void checkErrorMessageExists(String message) {
        mainPage.alertMessageLocator.should(visible);
        mainPage.alertMessageLocator.shouldHave(text(message));
    }
}
