package steps;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import org.testng.Assert;
import pages.RequestsPage;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.page;

public class RequestsPageSteps {

    private RequestsPage requestsPage = page(RequestsPage.class);

    @Step("Проверка работы сортировки по дате и времени")
    public void checkRequestsSorted() {
        requestsPage.clickSortByDate();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        List<LocalDateTime> requestsDateTime = requestsPage.getAllDisplayedRequestsDateTime().texts()
                .stream()
                .map(row -> LocalDateTime.parse(row, formatter))
                .collect(Collectors.toList());

        for(int i = 0; i < requestsDateTime.size() - 1; i++) {
            Assert.assertTrue(requestsDateTime.get(i).isAfter(requestsDateTime.get(i+1)),
                    String.format("datetime %s is after %s", requestsDateTime.get(i), requestsDateTime.get(i+1)));
        }
    }

    @Step("Проверка отсутствия сортировки")
    public void checkRequestsNotSorted() {

        requestsPage.clickDoNotSortButton();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        List<LocalDateTime> requestsDateTime = requestsPage.getAllDisplayedRequestsDateTime().texts()
                .stream()
                .map(row -> LocalDateTime.parse(row, formatter))
                .collect(Collectors.toList());

        boolean sorted = true;
        for(int i = 0; i < requestsDateTime.size() - 1; i++) {
            if(!requestsDateTime.get(i).isAfter(requestsDateTime.get(i+1)))
                sorted = false;
        }

        Assert.assertTrue(sorted, "requests must not be sorted");
    }

    @Step("Проверка существования запроса с текстом {0} в списке всех запросов")
    public void checkRequestDisplayed(String request) {
        requestsPage.clickSortByDate();
        requestsPage.getAllDisplayedRequestsTexts().filterBy(Condition.text(request))
                .shouldHave(CollectionCondition.sizeGreaterThan(0));
    }
}
