package steps;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import pages.SearchResultsPage;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.page;

public class SearchResultsSteps {

    private SearchResultsPage searchResultsPage = page(SearchResultsPage.class);

    @Step("Переход к главной странице")
    public void toMainPage() {
        searchResultsPage.toMainPageButtonClick();
    }

    @Step("Проверка правильности поиска по ключевым словам")
    public void checkRequestByKeywords(String value) {
        List<String> keywords = Arrays.asList(value.split(" "));

        for(String result : searchResultsPage.getAllDisplayedSearchResultsTexts().texts()) {
            for(String keyword : keywords)
                Assert.assertTrue(result.toUpperCase().contains(keyword.toUpperCase()),
                        String.format("search result with text '%s' doesnt contains keyword '%s'", result, keyword));
        }
    }

    @Step("Проверка количества результатов в выдаче поиска")
    public void checkResultsCount(int expectedCount) {
        Assert.assertEquals(searchResultsPage.getAllDisplayedSearchResultsTexts().size(), expectedCount,
                String.format("results count should be %d", expectedCount));
    }
}
